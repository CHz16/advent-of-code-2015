Day 3: Perfectly Spherical Houses in a Vacuum
=============================================

http://adventofcode.com/day/3

Given a starting position in a 2D grid and a list of instructions to move up, down, left, or right, count how many **unique** cell on the grid you visit. Instead of just allocating a 2D array like a normal person would, I decided to be "clever" and shove the coordinates of every visited cell into a set, which would take care of uniqueness for me.

This "cleverness" runs like garbage in a playground, so this one's a command like script.
