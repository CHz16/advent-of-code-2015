#!/usr/bin/env xcrun swift

//: Playground - noun: a place where people can play

import Cocoa

let url = URL(fileURLWithPath: "3.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func numberOfVisitedHouses(directions: String) -> Int {
    var houses : Set<String> = ["0,0"]

    var x = 0, y = 0
    for c in directions {
        switch c {
        case "^":
            y += 1
        case "v":
            y -= 1
        case ">":
            x += 1
        case "<":
            x -= 1
        default:
            break
        }
        houses.insert(String(format: "%d,%d", x, y))
    }

    return houses.count
}

print(numberOfVisitedHouses(directions: input))


// Part 2

func numberOfNextYearVisitedHouses(directions: String) -> Int {
    var houses : Set<String> = ["0,0"]

    var santaX = 0, santaY = 0, roboX = 0, roboY = 0
    var santasTurn =  true
    for c in directions {
        var xDiff = 0, yDiff = 0
        switch c {
        case "^":
            yDiff = 1
        case "v":
            yDiff = -1
        case ">":
            xDiff = 1
        case "<":
            xDiff = -1
        default:
            break
        }

        if santasTurn {
            santaX += xDiff
            santaY += yDiff
            houses.insert(String(format: "%d,%d", santaX, santaY))
            santasTurn = false
        } else {
            roboX += xDiff
            roboY += yDiff
            houses.insert(String(format: "%d,%d", roboX, roboY))
            santasTurn = true
        }
    }

    return houses.count
}

print(numberOfNextYearVisitedHouses(directions: input))
