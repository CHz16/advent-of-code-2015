Day 24: It Hangs in the Balance
===============================

http://adventofcode.com/day/24

3-Partition and 4-Partition. I found this to be the hardest problem besides day 19 part 2 to get working in a finite amount of execution time.

Solution outline:

1. Find the target sum (add up all the package weights and divide by the number of partitions).

2. Find the smallest number of packages that add to that sum (this is tiebreaker #1) and generate all combinations of that length.

3. Sort that list by their quantum (this is tiebreaker #2).

4. For every combination in the list, check if the packages are fully partitionable if one of the partitions in that combination.

5. The quantum of the first package for which the list is fully partitionable is the answer.

The source file will only solve one part of the puzzle at once; change line 6 to `let PARTITIONS = 3` for part 1 or `let PARTITIONS = 4` for part 2.
