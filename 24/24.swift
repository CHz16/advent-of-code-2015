#!/usr/bin/env xcrun swift

import Cocoa

let presents = Set([1, 3, 5, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113])
let PARTITIONS = 4
let TARGET = presents.reduce(0, +) / PARTITIONS


func combinationsTotaling(_ total: Int, length: Int, containers: [Int]) -> [[Int]] {
    func combinationsOfLength(_ length: Int, containers: [Int]) -> [[Int]] {
        var containers = containers

        if length == 0 {
            return [[]]
        }
        if containers.isEmpty {
            return []
        }

        let x = containers.popLast()!
        return combinationsOfLength(length, containers: containers) + combinationsOfLength(length - 1, containers: containers).map { $0 + [x] }
    }

    let c = combinationsOfLength(length, containers: containers).filter{ $0.reduce(0, +) == total }
    return c
}

func smallestCombinationsTotaling(_ total: Int, containers: [Int]) -> [[Int]] {
    var c: [[Int]]
    var n = 1
    repeat {
        c = combinationsTotaling(total, length: n, containers: containers)
        n += 1
    } while c.isEmpty
    return c
}

func quantum(_ s: [Int]) -> Int {
    return s.reduce(1) { $0 * $1 }
}

func partitionable(containers: Set<Int>, parts: Int, length: Int) -> Bool {
    if (parts == 1) {
        return true
    }

    for len in length...containers.count {
        let combinations = combinationsTotaling(TARGET, length: len, containers: Array(containers))
        if combinations.isEmpty {
            continue
        }

        for c in combinations {
            if partitionable(containers: containers.subtracting(c), parts: parts - 1, length: len) {
                return true
            }
        }
    }

    return false
}


let candidates = smallestCombinationsTotaling(TARGET, containers: Array(presents)).sorted { quantum($0) < quantum($1) }
let n = candidates[0].count
for candidate in candidates {
    print(quantum(candidate), candidate)
    if partitionable(containers: presents.subtracting(candidate), parts: PARTITIONS - 1, length: n) {
        break
    }
}
print("^^^ answer ^^^")
