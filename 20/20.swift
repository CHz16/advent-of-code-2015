#!/usr/bin/env xcrun swift

import Cocoa

let target = 33100000


// Parts 1 & 2

var foundHouseOne = false
var foundHouseTwo = false

var house = 1
while true {
    var sumOne = 0
    var sumTwo = 0
    for x in 1...Int(sqrt(Double(house))) {
        if (house % x) == 0 {
            let inv = house / x
            sumOne += x
            if (x != inv) {
                sumOne += inv
            }

            if (x * 50 >= house) {
                sumTwo += x
            }
            if (inv * 50 >= house && inv != x) {
                sumTwo += inv
            }
        }
    }

    if (!foundHouseOne && sumOne * 10 >= target) {
        print("1:", house, sumOne * 10)
        foundHouseOne = true
    }
    if (!foundHouseTwo && sumTwo * 11 >= target) {
        print("2:", house, sumTwo * 11)
        foundHouseTwo = true
    }
    if (foundHouseOne && foundHouseTwo) {
        break
    }

    house += 1
}
