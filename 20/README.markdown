Day 20: Infinite Elves and Infinite Houses
==========================================

http://adventofcode.com/day/20

Essentially: find the lowest number such that the sum of its divisors is greater than or equal to a target. Found the sums just by straightforward brute force: for `n`, run through every number `x` between `1` and `sqrt(n)` and add both `x` and `n/x` to the sum (unless `x` = `n/x`, in which case you only add `x`).
