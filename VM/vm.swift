#!/usr/bin/env xcrun swift

import Cocoa

struct VM {
    var memory = Array(repeating: 0, count: 32768 + 8)
    var stack: [Int] = []
    var pc = 0
    var inputBuffer = ""

    init() {
        let url = URL(fileURLWithPath: "challenge.bin")
        let data = try! Data(contentsOf: url)

        var b = 0
        data.withUnsafeBytes { bytes -> Void in
            while b < bytes.count {
                let byte1 = Int(bytes[b])
                let byte2 = Int(bytes[b + 1])
                memory[b / 2] = byte1 + byte2 << 8
                b += 2
            }
        }
    }


    typealias Register = Int
    enum Instruction {
        case Halt, Set(Register, Value), Push(Value), Pop(Register), Eq(Register, Value, Value)
        case Gt(Register, Value, Value), Jmp(Value), Jt(Value, Value), Jf(Value, Value), Add(Register, Value, Value)
        case Mult(Register, Value, Value), Mod(Register, Value, Value), And(Register, Value, Value), Or(Register, Value, Value), Not(Register, Value)
        case Rmem(Register, Value), Wmem(Value, Value), Call(Value), Ret, Out(Value)
        case In(Register), Noop
    }

    enum Value {
        case Literal(Int), Register(Int)

        init(i: Int) {
            if i > 32767 {
                self = .Register(i)
            } else {
                self = .Literal(i)
            }
        }
    }

    func unboxValue(_ v: Value) -> Int {
        switch v {
        case let .Register(r):
            return memory[r]
        case let .Literal(i):
            return i
        }
    }


    mutating func run() {
        while true {
            guard let instruction = readInstruction() else {
                if pc >= 32768 {
                    print("Exit: program counter is past executable memory")
                } else {
                    print("Exit: invalid instruction", memory[pc - 1], "read at address", pc - 1)
                }
                return
            }

            switch instruction {
            case .Halt:
                print("Exit: halt instruction")
                return
            case let .Set(register, value):
                write(unboxValue(value), toLocation: register)
            case let .Push(value):
                stack.append(unboxValue(value))
            case let .Pop(register):
                write(stack.popLast()!, toLocation: register)
            case let .Eq(register, value1, value2):
                let b = (unboxValue(value1) == unboxValue(value2)) ? 1 : 0
                write(b, toLocation: register)
            case let .Gt(register, value1, value2):
                let b = (unboxValue(value1) > unboxValue(value2)) ? 1 : 0
                write(b, toLocation: register)
            case let .Jmp(locationValue):
                pc = unboxValue(locationValue)
            case let .Jt(value, locationValue):
                if unboxValue(value) != 0 {
                    pc = unboxValue(locationValue)
                }
            case let .Jf(value, locationValue):
                if unboxValue(value) == 0 {
                    pc = unboxValue(locationValue)
                }
            case let .Add(register, value1, value2):
                write(unboxValue(value1) + unboxValue(value2), toLocation: register)
            case let .Mult(register, value1, value2):
                write(unboxValue(value1) * unboxValue(value2), toLocation: register)
            case let .Mod(register, value1, value2):
                write(unboxValue(value1) % unboxValue(value2), toLocation: register)
            case let .And(register, value1, value2):
                write(unboxValue(value1) & unboxValue(value2), toLocation: register)
            case let .Or(register, value1, value2):
                write(unboxValue(value1) | unboxValue(value2), toLocation: register)
            case let .Not(register, value):
                write(~unboxValue(value), toLocation: register)
            case let .Rmem(register, value):
                write(memory[unboxValue(value)], toLocation: register)
            case let .Wmem(addressValue, value):
                write(unboxValue(value), toLocation: unboxValue(addressValue))
            case let .Call(value):
                stack.append(pc)
                pc = unboxValue(value)
            case .Ret:
                pc = stack.popLast()!
            case let .Out(value):
                print(Character(UnicodeScalar(unboxValue(value))!), terminator: "")
            case let .In(register):
                if inputBuffer == "" {
                    let stdin = FileHandle.standardInput
                    inputBuffer = String(data: stdin.availableData, encoding: String.Encoding.utf8)!
                }
                memory[register] = Int((inputBuffer.unicodeScalars.first?.value)!)
                inputBuffer = String(inputBuffer.dropFirst())
            case .Noop:
                break
            }
        }
    }

    mutating func readInstruction() -> Instruction? {
        guard pc < 32768 else {
            return nil
        }

        pc += 1
        switch memory[pc - 1] {
        case 0: return .Halt
        case 1: pc += 2; return .Set(memory[pc - 2], Value(i: memory[pc - 1]))
        case 2: pc += 1; return .Push(Value(i: memory[pc - 1]))
        case 3: pc += 1; return .Pop(memory[pc - 1])
        case 4: pc += 3; return .Eq(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 5: pc += 3; return .Gt(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 6: pc += 1; return .Jmp(Value(i: memory[pc - 1]))
        case 7: pc += 2; return .Jt(Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 8: pc += 2; return .Jf(Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 9: pc += 3; return .Add(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 10: pc += 3; return .Mult(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 11: pc += 3; return .Mod(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 12: pc += 3; return .And(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 13: pc += 3; return .Or(memory[pc - 3], Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 14: pc += 2; return .Not(memory[pc - 2], Value(i: memory[pc - 1]))
        case 15: pc += 2; return .Rmem(memory[pc - 2], Value(i: memory[pc - 1]))
        case 16: pc += 2; return .Wmem(Value(i: memory[pc - 2]), Value(i: memory[pc - 1]))
        case 17: pc += 1; return .Call(Value(i: memory[pc - 1]))
        case 18: return .Ret
        case 19: pc += 1; return .Out(Value(i: memory[pc - 1]))
        case 20: pc += 1; return .In(memory[pc - 1])
        case 21: return .Noop
        default: return nil
        }
    }

    mutating func write(_ i: Int, toLocation address: Int) {
        memory[address] = i & 32767
    }
}

var vm = VM()
vm.run()
