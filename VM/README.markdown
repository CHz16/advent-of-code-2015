Synacor Challenge - VM
======================

Solution to the first part of the [Synacor Challenge](https://challenge.synacor.com): a VM for a fake machine architecture. See `arch-spec` for the specifications of the machine's hardware and instruction set.

There's a lot more to this challenge than just this VM, but I quit 3/4 of the way through because assembly hackery is decidedly not my thing.
