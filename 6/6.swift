#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "6.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var grid = Array(repeating: 0, count: 1000 * 1000)

for instruction in input.components(separatedBy: "\n") {
    let command = instruction[..<instruction.index(instruction.startIndex, offsetBy: 7)]
    let coords = instruction.components(separatedBy: CharacterSet.decimalDigits.inverted).filter { $0 != "" }

    for x in Int(coords[0])!...Int(coords[2])! {
        for y in Int(coords[1])!...Int(coords[3])! {
            let i = x * 1000 + y
            switch command {
            case "turn on":
                grid[i] = 1
            case "turn of":
                grid[i] = 0
            case "toggle ":
                grid[i] = 1 - grid[i]
            default:
                break
            }
        }
    }
}
print(grid.reduce(0) { (total, n) in total + n })


// Part 2

var grid2 = Array(repeating: 0, count: 1000 * 1000)

for instruction in input.components(separatedBy: "\n") {
    let command = instruction[..<instruction.index(instruction.startIndex, offsetBy: 7)]
    let coords = instruction.components(separatedBy: CharacterSet.decimalDigits.inverted).filter { $0 != "" }

    for x in Int(coords[0])!...Int(coords[2])! {
        for y in Int(coords[1])!...Int(coords[3])! {
            let i = x * 1000 + y
            switch command {
            case "turn on":
                grid2[i] += 1
            case "turn of":
                grid2[i] = max(grid2[i] - 1, 0)
            case "toggle ":
                grid2[i] += 2
            default:
                break
            }
        }
    }
}
print(grid2.reduce(0) { (total, n) in total + n })
