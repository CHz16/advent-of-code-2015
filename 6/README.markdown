Day 6: Probably a Fire Hazard
=============================

http://adventofcode.com/day/6

Given a list of instructions to turn off, turn on, or toggle arbitrary rectangular areas of lights in a 1000x1000 grid, count the sum of the intensity values of each light after executing the instructions.

I just did this the simplest way and processed each individual light in each rectangular area separately, which is slower than it might've been if I'd been able to come up with a way to mass-process whole regions at once.
