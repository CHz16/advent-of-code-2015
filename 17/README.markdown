Day 17: No Such Thing as Too Much
=================================

http://adventofcode.com/day/17

Given a list of integers and a target number, find how many combinations of elements in the list add up to the target. Another brute force problem with a recursive generator from me: find all possible combinations, count the lengths of each, and collect the results into a dictionary.

`tabulate(combinations: [[Int]]) -> [Int: Int]` takes a list of lists of integers and returns a dictionary where for every (`key`, `value`) in the dictionary, `value` is the number of lists of length `key` in the input. It's really easy to answer parts 1 & 2 with this info.

Instead of implementing `tabulate` with a for loop or something like a normal person, I did it with `map` and `reduce`??? Why did I do this. I like the idea of collecting into something more complicated than a primitive with `reduce` because that's a really powerful technique, but wow I went overboard with this.

Note to my future self: next time I do this, make the dictionary an `inout` parameter so it's not copied every loop.
