#!/usr/bin/env xcrun swift

import Cocoa


// Parts 1 & 2

let containers = [43, 3, 4, 10, 21, 44, 4, 6, 47, 41, 34, 17, 17, 44, 36, 31, 46, 9, 27, 38]

func combinationsTotaling(_ total: Int, containers: [Int]) -> [[Int]] {
    if total == 0 {
        return [[]]
    }
    if containers.isEmpty {
        return []
    }

    var rest = containers
    let x = rest.popLast()!
    return combinationsTotaling(total, containers: rest) + combinationsTotaling(total - x, containers: rest).map { $0 + [x] }
}

func tabulate(_ combinations: [[Int]]) -> [Int: Int] {
    return combinations.map { $0.count }.reduce(Dictionary<Int, Int>()) {
        (_d, x) in
        var d = _d
        if d[x] == nil {
            d[x] = 1
        } else {
            d[x]! += 1
        }
        return d
    }
}

let t = tabulate(combinationsTotaling(150, containers: containers))
print(t)
print(t.values.reduce(0) { $0 + $1 })
print(t[t.keys.min()!]!)
