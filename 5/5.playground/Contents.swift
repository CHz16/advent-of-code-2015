//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func isNiceString(_ s: String) -> Bool {
    var vowels = 0
    var foundDouble = false

    var index = s.startIndex
    while index != s.index(s.endIndex, offsetBy: -1) {
        let pair = s[index..<s.index(index, offsetBy: 2)]
        if (pair == "ab" || pair == "cd" || pair == "pq" || pair == "xy") {
            return false
        }

        if ("aeiou".firstIndex(of: s[index]) != nil) {
            vowels += 1
        }

        if s[index] == s[s.index(after: index)] {
            foundDouble = true
        }

        index = s.index(after: index)
    }

    if ("aeiou".firstIndex(of: s[index]) != nil) {
        vowels += 1
    }

    return (vowels >= 3) && foundDouble
}

// test cases
isNiceString("ugknbfddgicrmopn")
isNiceString("aaa")
isNiceString("jchzalrnumimnmhp")
isNiceString("haegwjzuvuyypxyu")
isNiceString("dvszwmarrgswjxmb")

// solution
var c = 0
for s in input.components(separatedBy: "\n") {
    if isNiceString(s) {
        c += 1
    }
}
c


// Part 2

func isNiceString2(_ s: String) -> Bool {
    var foundPair = false, foundTriple = false

    var index = s.startIndex
    while index != s.index(s.endIndex, offsetBy: -2) {
        if !foundTriple {
            if s[index] == s[s.index(index, offsetBy: 2)] {
                foundTriple = true
            }
        }

        if !foundPair {
            let pair = s[index..<s.index(index, offsetBy: 2)]
            if s[s.index(index, offsetBy: 2)...].range(of: pair) != nil {
                foundPair = true
            }
        }

        index = s.index(after: index)
    }

    return foundPair && foundTriple
}

// test cases
isNiceString2("qjhvhtzxzqqjkmpb")
isNiceString2("xxyxx")
isNiceString2("uurcxstgmygtbstg")
isNiceString2("ieodomkazucvgmuy")

// solution
var c2 = 0
for s in input.components(separatedBy: "\n") {
    if isNiceString2(s) {
        c2 += 1
    }
}
c2
