Day 5: Doesn't He Have Intern-Elves For This?
=============================================

http://adventofcode.com/day/5

A relatively simple string processing problem, just the fastest implementations I could slap down of checking whether strings meet certain conditions.
