//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


let targets = ["children": 3, "cats": 7, "samoyeds": 2, "pomeranians": 3, "akitas": 0, "vizslas": 0, "goldfish": 5, "trees": 3, "cars": 2, "perfumes": 1]


// Part 1

sue: for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    let attributes: [String: Int] = [components[2]: (Int(components[3])!), components[4]: (Int(components[5])!), components[6]: (Int(components[7])!)]

    for (attribute, value) in attributes {
        if targets[attribute] != value {
            continue sue
        }
    }

    print(line)
}



// Part 2

sue2: for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    let attributes: [String: Int] = [components[2]: (Int(components[3])!), components[4]: (Int(components[5])!), components[6]: (Int(components[7])!)]

    for (attribute, value) in attributes {
        if attribute == "cats" || attribute == "trees" {
            if targets[attribute]! >= value {
                continue sue2
            }
        }
        else if attribute == "pomeranians" || attribute == "goldfish" {
            if targets[attribute]! <= value {
                continue sue2
            }
        } else {
            if targets[attribute] != value {
                continue sue2
            }
        }
    }

    print(line)
}
