Day 16: Aunt Sue
================

http://adventofcode.com/day/16

Given a set of ten conditions and a list of globs of data, find the one glob that doesn't fail any of the conditions. I just go line-by-line and test every condition, pretty simple.
