#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "18.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


func stepLife(_ grid: [[Character]], partTwo: Bool = false) -> [[Character]] {
    func aliveNeighbors(row: Int, col: Int) -> Int {
        var neighbors = [(row - 1, col - 1), (row - 1, col), (row - 1, col + 1), (row, col - 1), (row, col + 1), (row + 1, col - 1), (row + 1, col), (row + 1, col + 1)]
        neighbors = neighbors.filter {
            (row, col) in return (row >= 0) && (col >= 0) && (row < grid.count) && (col < grid[0].count)
        }

        var n = 0
        for (r, c) in neighbors {
            if grid[r][c] == "#" {
                n += 1
            }
        }
        return n
    }

    var newGrid = grid

    for row in 0..<grid.count {
        for col in 0..<grid[row].count {
            let n = aliveNeighbors(row: row, col: col)

            if grid[row][col] == "#" {
                if n != 2 && n != 3 {
                    newGrid[row][col] = "."
                }
            } else {
                if n == 3 {
                    newGrid[row][col] = "#"
                }
            }
        }
    }

    if (partTwo) {
        newGrid[0][0] = "#"
        newGrid[grid.count - 1][0] = "#"
        newGrid[0][grid[0].count - 1] = "#"
        newGrid[grid.count - 1][grid[0].count - 1] = "#"
    }

    return newGrid
}


// Part 1

var grid: [[Character]] = []
for line in input.components(separatedBy: "\n") {
    grid.append(Array(line))
}

for _ in 0..<100 {
    grid = stepLife(grid)
}
print(grid.reduce([], +).filter { $0 == "#" }.count)


// Part 2

grid = []
for line in input.components(separatedBy: "\n") {
    grid.append(Array(line))
}
grid[0][0] = "#"
grid[grid.count - 1][0] = "#"
grid[0][grid[0].count - 1] = "#"
grid[grid.count - 1][grid[0].count - 1] = "#"

for _ in 0..<100 {
    grid = stepLife(grid, partTwo: true)
}
print(grid.reduce([], +).filter { $0 == "#" }.count)
