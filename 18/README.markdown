Day 18: Like a GIF For Your Yard
================================

http://adventofcode.com/day/18

It's the Game of Life! Everyone loves Life

Nothing really of note to comment on here, except maybe my implementation of the `aliveNeighbors()` nested inside `stepLife()`. I see that I tried to be clever by using `filter` instead of having a bunch of `if` statements checking whether the neighboring cells were in bounds or not, that's cool. But what confuses me is that in the second half when the function actually checks the values of the neighbors, I used a `for` loop instead of going all the way and `map`ping or something. For some reason, I gave up with the higher-order functions and just switched to C mode.

Like, here's what I was expecting to see:

    func aliveNeighbors(row: Int, col: Int) -> Int {
        var neighbors = [(row - 1, col - 1), (row - 1, col), (row - 1, col + 1), (row, col - 1), (row, col + 1), (row + 1, col - 1), (row + 1, col), (row + 1, col + 1)]
        neighbors = neighbors.filter {
            (row, col) in return (row >= 0) && (col >= 0) && (row < grid.count) && (col < grid[0].count)
        }

        return neighbors.reduce(0) { (s: Int, pos: (Int, Int)) in s + (grid[pos.0][pos.1] == "#" ? 1 : 0) }
    }
