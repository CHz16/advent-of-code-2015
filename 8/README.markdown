Day 8: Matchsticks
==================

http://adventofcode.com/day/8

Back to another pretty simple string processing problem. Nothing sophisticated here; we just go through one character at a time and update our counters as necessary.
