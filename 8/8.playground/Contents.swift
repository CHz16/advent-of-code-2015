//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var skippedCount = 0
var i = input.startIndex
while i != input.endIndex {
    switch input[i] {
    case "\n":
        break
    case "\"":
        skippedCount += 1
    case "\\":
        i = input.index(after: i)
        skippedCount += 1
        if (input[i] == "x") {
            skippedCount += 2
            i = input.index(i, offsetBy: 2)
        }
    default:
        break
    }
    i = input.index(after: i)
}
skippedCount


// Part 2

var extraCount = 0
for line in input.components(separatedBy: "\n") {
    extraCount += 2
    for c in line {
        if (c == "\"" || c == "\\") {
            extraCount += 1
        }
    }
}
extraCount
