Day 12: JSAbacusFramework.io
============================

http://adventofcode.com/day/12

Relatively simple JSON parsing problem.

For the first part, I have a one-line monstrosity that totally ignores the structure of the JSON document and instead uses the excellent String.componentsSeparatedByCharactersInSet to split the raw text into an array of all the numbers in it, which is then summed.

The second part is done Properly.
