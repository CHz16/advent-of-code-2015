//: Playground - noun: a place where people can play

import Cocoa
import Foundation

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let sum = input.components(separatedBy: CharacterSet(charactersIn: "-0123456789").inverted).filter { !$0.isEmpty }.reduce(0) { $0 + Int($1)! }
sum


// Part 2

func redlessSum(_ obj: Any) -> Int {
    if obj is Int {
        return obj as! Int
    } else if obj is Array<AnyObject> {
        let a = obj as! Array<AnyObject>
        return a.reduce(0) { $0 + redlessSum($1) }
    } else if obj is Dictionary<String, AnyObject> {
        let d = obj as! Dictionary<String, AnyObject>
        for v in d.values {
            if v is String && v as! String == "red" {
                return 0
            }
        }
        return d.values.reduce(0) { $0 + redlessSum($1) }
    }
    return 0
}

let json = try JSONSerialization.jsonObject(with: Data(contentsOf: url!), options: [])
redlessSum(json)
