Day 2: I Was Told There Would Be No Math
========================================

http://adventofcode.com/day/2

Extremely simple math problem: just loop through all the lines in the file, do some arithmetic on each one, and sum up the answers. Just implemented this one straightforwardly and imperatively.
