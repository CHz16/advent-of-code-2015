//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func necessaryPaper(length: Int, width: Int, height: Int) -> Int {
    let side1 = length * width
    let side2 = width * height
    let side3 = height * length
    return 2 * side1 + 2 * side2 + 2 * side3 + min(side1, side2, side3)
}

// test cases
necessaryPaper(length: 2, width: 3, height: 4)
necessaryPaper(length: 1, width: 1, height: 10)

// solution
var squareFeet = 0
for box in input.components(separatedBy: "\n") {
    let sides = box.components(separatedBy: "x")
    squareFeet += necessaryPaper(length: Int(sides[0])!, width: Int(sides[1])!, height: Int(sides[2])!)
}
squareFeet


// Part 2

func necessaryRibbon(length: Int, width: Int, height: Int) -> Int {
    return 2 * (length + width + height - max(length, width, height)) + length * width * height
}

// test cases
necessaryRibbon(length: 2, width: 3, height: 4)
necessaryRibbon(length: 1, width: 1, height: 10)

// solution
var ribbon = 0
for box in input.components(separatedBy: "\n") {
    let sides = box.components(separatedBy: "x")
    ribbon += necessaryRibbon(length: Int(sides[0])!, width: Int(sides[1])!, height: Int(sides[2])!)
}
ribbon
