Advent of Code 2015
===================

These are my solutions in Swift to [Advent of Code](http://adventofcode.com/2015), a kind of fun series of 25 programming puzzles that ran in December 2015. I figured I'd throw these up on the web somewhere, because why not.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you.

I started writing every single challenge in a playground, because they're fantastic tools, but about half of them I ended up running from the command line after finishing the algorithm because it was way faster. Usually this was because something was looping thousands of times and timelines collecting all those intermediate values massively slowed things down. So if you see a playground solution, that means it ran well enough that I didn't have to drop down and get a boost with `xcrun`.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

"Good" solutions I'm not totally embarrassed of: 7, 19, 24, 25

"Bad" solutions I **am** totally embarrassed of: 4, 21, 22

"Why did I do this" solutions where my own code disturbs me: 12, 17

*See also: [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*


VM
--

Also included is my solution to the first part of the [Synacor Challenge](https://challenge.synacor.com): a VM for a fake machine architecture. It's only tangentially related to Advent of Code, in that both were designed by the same person ([Eric Wastl](http://was.tl)), but my day 23 solution is basically just a copy-paste of the Synacor VM, so I figured I'd toss it in here too.

I wrote the VM over a couple of days without any time pressure, so its code is a lot better than my Advent of Code solutions. :J
