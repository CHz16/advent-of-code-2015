Day 11: Corporate Policy
========================

http://adventofcode.com/day/11

This is very similar to day 5: check if a string matches certain conditions. The only difference besides the conditions is, instead of just checking of a bunch of set strings are valid, you have one string and you keep lexicographically "incrementing" it until you find another string that's valid.

My implementation here is really straightforward, except because of Swift's incredibly smart string handling, I found it easier to decompose the string into an array of integers when working with it and compose it back to a string at the end.
