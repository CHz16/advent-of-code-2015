#!/usr/bin/env xcrun swift

import Cocoa

let chars = ["", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
let ints: [Character: Int] = ["a": 1, "b": 2, "c": 3, "d": 4, "e": 5, "f": 6, "g": 7, "h": 8, "i": 9, "j": 10, "k": 11, "l": 12, "m": 13, "n": 14, "o": 15, "p": 16, "q": 17, "r": 18, "s": 19, "t": 20, "u": 21, "v": 22, "w": 23, "x": 24, "y": 25, "z": 26]

func passwordToArray(_ password: String) -> [Int] {
    var a: Array<Int> = []
    for c in password {
        a.append(ints[c]!)
    }
    return a
}

func arrayToPassword(_ array: [Int]) -> String {
    var s = ""
    for i in array {
        s += chars[i]
    }
    return s
}

func isValidPassword(_ array: [Int]) -> Bool {
    // Condition 1
    var triple = false
    for i in 0..<(array.count - 2) {
        if (array[i] + 1) == array[i + 1] {
            if (array[i + 1] + 1) == array[i + 2] {
                triple = true
            }
        }
    }
    if !triple {
        return false
    }

    // Condition 2
    for i in array {
        if i == 9 || i == 15 || i == 12 {
            return false
        }
    }

    // Condition 3
    var pairs = 0
    var i = 0
    while i < (array.count - 1) {
        if array[i] == array[i + 1] {
            pairs += 1
            i += 1
        }
        i += 1
    }
    if pairs < 2 {
        return false
    }

    return true
}

func incrementPassword(_ a: [Int]) -> [Int] {
    func incrementCharacterInString(_ a: [Int], index: Int) -> [Int] {
        var s = a
        if s[index] == 26 {
            s[index] = 1
            return incrementCharacterInString(s, index: index - 1)
        } else {
            s[index] += 1
            return s
        }
    }
    return incrementCharacterInString(a, index: a.count - 1)
}

func newPassword(_ password: String) -> String {
    var a = passwordToArray(password)

    repeat {
        a = incrementPassword(a)
    } while !isValidPassword(a)

    return arrayToPassword(a)
}


// Parts 1 & 2

let firstNewPassword = newPassword("vzbxkghb")
print(firstNewPassword)
print(newPassword(firstNewPassword))
