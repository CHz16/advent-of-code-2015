#!/usr/bin/env xcrun swift

import Cocoa


enum Spell: Int { case MagicMissile = 0, Drain, Shield, Poison, Recharge }
let SPELL_COUNT = 5
let SPELL_COSTS = [53, 73, 113, 173, 229]
let MIN_SPELL_COST = SPELL_COSTS.min()!

struct SpellSequenceGenerator {
    var state: [Int]
    var looped = false

    init(length: Int) {
        state = Array(repeating: 0, count: length)
    }

    mutating func nextSequence() -> [Spell]? {
        var sequence: [Spell] = []
        gen: while true {
            if looped {
                return nil
            }

            sequence = state.map { Spell(rawValue: $0)! }

            var i = state.count - 1
            repeat {
                state[i] = (state[i] + 1) % SPELL_COUNT
                i -= 1
            } while i >= 0 && state[i + 1] == 0

            if (i < 0 && state[0] == 0) {
                looped = true
            }

            var shieldDuration = 0, poisonDuration = 0, rechargeDuration = 0
            for spell in sequence {
                shieldDuration -= 1
                poisonDuration -= 1
                rechargeDuration -= 1

                switch spell {
                case .Shield:
                    if shieldDuration > 0 {
                        continue gen
                    }
                    shieldDuration = 3
                case .Poison:
                    if poisonDuration > 0 {
                        continue gen
                    }
                    poisonDuration = 3
                case .Recharge:
                    if rechargeDuration > 0 {
                        continue gen
                    }
                    rechargeDuration = 3
                default:
                    break
                }
            }

            break
        }

        return sequence
    }
}


let PLAYER_HP = 50, PLAYER_MANA = 500
let BOSS_HP = 51, BOSS_DAMAGE = 9


// Part 1

var minMana = Int.max
var length = 1
game: while true {
    if minMana < length * MIN_SPELL_COST {
        break
    }

    print("current length", length, "/ max depth:", minMana / MIN_SPELL_COST)

    var gen = SpellSequenceGenerator(length: length)
    seq: while let sequence = gen.nextSequence() {
        let spentMana = sequence.reduce(0) { $0 + SPELL_COSTS[$1.rawValue] }
        if (spentMana >= minMana) {
            continue
        }

        var playerHP = PLAYER_HP, playerMana = PLAYER_MANA
        var bossHP = BOSS_HP, bossDamage = BOSS_DAMAGE
        var shieldDuration = 0, poisonDuration = 0, rechargeDuration = 0

        for spell in sequence {
            playerMana -= SPELL_COSTS[spell.rawValue]
            if playerMana < 0 {
                continue seq
            }

            // Player Turn

            //print("-- Player Turn--")
            //print("Player has", playerHP, "HP, boss has", bossHP, "HP")

            playerHP -= 1
            if (playerHP <= 0) {
                continue seq
            }

            shieldDuration -= 1
            if shieldDuration == 0 {
                bossDamage = BOSS_DAMAGE
                //print("Shield expires")
            } else if shieldDuration > 0 {
                //print("Shield's duration is", shieldDuration)
            }

            poisonDuration -= 1
            if poisonDuration == 0 {
                //print("Poison expires")
            } else if poisonDuration > 0 {
                bossHP -= 3
                //print("Poison's duration is", poisonDuration, ": boss takes 3 damage, now at", bossHP)
                if bossHP <= 0 {
                    //print("Boss is dead")
                    if spentMana < minMana {
                        print(spentMana, sequence)
                        minMana = spentMana
                    }
                    continue seq
                }
            }

            rechargeDuration -= 1
            if rechargeDuration == 0 {
                //print("Recharge expires")
            } else if rechargeDuration > 0 {
                playerMana += 101
                //print("Recharge's duration is", rechargeDuration, ": player gaines 101 mana, now at", playerMana)
            }

            switch spell {
            case .MagicMissile:
                bossHP -= 4
                //print("Player casts Magic Missile: boss takes 4 damage, now at", bossHP)
                if bossHP <= 0 {
                    //print("Boss is dead")
                    if spentMana < minMana {
                        print(spentMana, sequence)
                        minMana = spentMana
                    }
                    continue seq
                }
            case .Drain:
                bossHP -= 2
                playerHP += 2
                //print("Player casts Drain: player at", playerHP, "HP, boss at ", bossHP, "HP")
                if bossHP <= 0 {
                    //print("Boss is dead")
                    if spentMana < minMana {
                        print(spentMana, sequence)
                        minMana = spentMana
                    }
                    continue seq
                }
            case .Shield:
                //print("Player casts Shield")
                shieldDuration = 6
                bossDamage -= 7
            case .Poison:
                //print("Player casts Poison")
                poisonDuration = 7
            case .Recharge:
                //print("Player casts Recharge")
                rechargeDuration = 6
            }

            //print("")

            // Boss Turn

            //print("-- Boss Turn--")
            //print("Player has", playerHP, "HP, boss has", bossHP, "HP")

            shieldDuration -= 1
            if shieldDuration == 0 {
                bossDamage = BOSS_DAMAGE
                //print("Shield expires")
            } else if shieldDuration > 0 {
                //print("Shield's duration is", shieldDuration)
            }

            poisonDuration -= 1
            if poisonDuration == 0 {
                //print("Poison expires")
            } else if poisonDuration > 0 {
                bossHP -= 3
                //print("Poison's duration is", poisonDuration, ": boss takes 3 damage, now at", bossHP)
                if bossHP <= 0 {
                    //print("Boss is dead")
                    if spentMana < minMana {
                        print(spentMana, sequence)
                        minMana = spentMana
                    }
                    continue seq
                }
            }

            rechargeDuration -= 1
            if rechargeDuration == 0 {
                //print("Recharge expires")
            } else if rechargeDuration > 0 {
                playerMana += 101
                //print("Recharge's duration is", rechargeDuration, ": player gaines 101 mana, now at", playerMana)
            }

            playerHP -= bossDamage
            //print("Boss attacks: player takes", bossDamage, "damage, now at", playerHP)
            
            if (playerHP <= 0) {
                //print("Player is dead")
                continue seq
            }
            
            //print("")
        }
    }
    
    length += 1
}
