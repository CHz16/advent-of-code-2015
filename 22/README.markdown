Day 22: Wizard Simulator 20XX
=============================

http://adventofcode.com/day/22

Harder fake RPG problem than day 21 where you need to find the sequence of spells with the least mana cost to defeat a boss. The part that makes this puzzle really tricky is that there's one spell that restores mana. There are two separate files: `22-1.swift` runs part 1, and `22-2.swift` has the couple of minor modifications for part 2.

My solution for this is extremely bad: brute force by generating all "reasonable" sequences of spells of all lengths, until the length of the solution is so long that it must cost more mana than whatever the best solution we've found is. The search space is so large that both of these actually fail to execute; I only got this problem right because they output solutions as they find them and the first solutions both found happened to be correct.

A much better and faster solution would've been to do some tree searching with pruning of branches when someone dies.

The only saving grace of this solution is that I kind of like the generator. For once during this contest, I wrote an actual generator in the real sense of the word that only yields one sequence at a time. It even knows the durations of spells that have effects over multiple turns and won't produce any "unreasonable" sequences of spells that would waste a turn by recasting while an effect was already active.
