Day 13: Knights of the Dinner Table
===================================

http://adventofcode.com/day/13

This is just a disguised traveling salesman problem, with the change that the "distance" between two "cities" is the change in happiness A would get sitting next to B plus the change in happiness B would get sitting to A. I just directly reused the code from day 9 without even changing variable names.
