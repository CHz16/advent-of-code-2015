#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "13.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


func permutations<T>(_ l: [T]) -> [[T]] {
    if l.count == 0 {
        return [[]]
    }

    var perms: [[T]] = []
    for i in 0..<l.count {
        let l2 = Array(l[0..<i] + l[i+1..<l.count])
        perms += permutations(l2).map { $0 + [l[i]] }
    }
    return perms
}


var distances: Dictionary<String, Dictionary<String, Int>> = [:]

for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    let city1 = components[0], city2 = components[9], distance = Int(components[2])!

    if distances[city1] == nil {
        distances[city1] = [:]
    }
    distances[city1]?[city2] = distance
}


// Part 1

var maxRouteDistance = Int.min
for route in permutations(Array(distances.keys)) {
    var distance = 0
    for i in 0..<(route.count - 1) {
        distance += (distances[route[i]]?[route[i + 1]])!
        distance += (distances[route[i + 1]]?[route[i]])!
    }
    distance += (distances[route[0]]?[route[route.count - 1]])!
    distance += (distances[route[route.count - 1]]?[route[0]])!
    if distance > maxRouteDistance {
        maxRouteDistance = distance
    }
}
print(maxRouteDistance)


// Part 2

distances["Me"] = [:]
let guests = distances.keys
for guest in guests {
    distances["Me"]?[guest] = 0
    distances[guest]?["Me"] = 0
}

var maxRouteDistance2 = Int.min
for route in permutations(Array(distances.keys)) {
    var distance = 0
    for i in 0..<(route.count - 1) {
        distance += (distances[route[i]]?[route[i + 1]])!
        distance += (distances[route[i + 1]]?[route[i]])!
    }
    distance += (distances[route[0]]?[route[route.count - 1]])!
    distance += (distances[route[route.count - 1]]?[route[0]])!
    if distance > maxRouteDistance2 {
        maxRouteDistance2 = distance
    }
}
print(maxRouteDistance2)
