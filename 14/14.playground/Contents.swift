//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

struct Reindeer {
    let speed, duration, rest: Int
}

var reindeers: [String: Reindeer] = [:]
for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    reindeers[components[0]] = Reindeer(speed: Int(components[3])!, duration: Int(components[6])!, rest: Int(components[13])!)
}

var locs: [String: Int] = [:]
var points: [String: Int] = [:]
var states: [String: Int] = [:]
for (name, reindeer) in reindeers {
    locs[name] = 0
    points[name] = 0
    states[name] = reindeer.duration
}

var t = 0
while t < 2503 {
    for (name, reindeer) in reindeers {
        if states[name]! > 0 {
            locs[name]! += reindeer.speed
        }
        states[name] = states[name]! - 1
        if states[name]! < -(reindeer.rest - 1) {
            states[name] = reindeer.duration
        }
    }

    var maxVal = 0
    for (name, _) in reindeers {
        if locs[name]! > maxVal {
            maxVal = locs[name]!
        }
    }
    for (name, _) in reindeers {
        if locs[name] == maxVal {
            points[name]! += 1
        }
    }

    t += 1
}
print(locs)
print(points)
