Day 14: Reindeer Olympics
=========================

http://adventofcode.com/day/14

I just straightforwardly implemented this one by making Reindeer objects for each reindeer that keep their stats and whether they're currently flying or resting, as well as the locations and scores of each one. Nothing tricky.
