//: Playground - noun: a place where people can play

import Cocoa

let initial = 20151125
let base = 252533
let modulus = 33554393

let row = 2978
let column = 3083

// Part 1

var exponent = (column * (column + 1) + (row + column - 1) * ((row + column - 1) - 1) - (column - 1) * column) / 2 - 1
var n = 1
var b = base
while exponent > 0 {
    if exponent % 2 == 1 {
        n = (n * b) % modulus
    }
    exponent /= 2
    b = (b * b) % modulus
}
print((initial * n) % modulus)
