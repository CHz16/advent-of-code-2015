Day 25: Let It Snow
===================

http://adventofcode.com/day/25

We end with modular exponentiation, which is an odd close after day 24's NP-complete hellscape.

I originally did this the long way by just tossing it at the command-line interpreter and letting it do 18 million multiplications, but for funsies after that I implemented exponentiation by squaring so that it could run instantaneously in a playground.
