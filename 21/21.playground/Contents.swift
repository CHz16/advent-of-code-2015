//: Playground - noun: a place where people can play

import Cocoa


typealias Equipment = (String, Int, Int, Int)

let weapons = [("Dagger", 8, 4, 0), ("Shortsword", 10, 5, 0), ("Warhammer", 25, 6, 0), ("Longsword", 40, 7, 0), ("Greataxe", 74, 8, 0)]
let armor = [("None", 0, 0, 0), ("Leather", 13, 0, 1), ("Chainmail", 31, 0, 2), ("Splintmail", 53, 0, 3), ("Bandedmail", 75, 0, 4), ("Platemail", 102, 0, 5)]
let rings = [("Damage +1", 25, 1, 0), ("Damage +2", 50, 2, 0), ("Damage +3", 100, 3, 0), ("Defense +1", 20, 0, 1), ("Defense +2", 40, 0, 2), ("Defense +3", 80, 0, 3)]

func sumEquipment(_ equipment: [Equipment]) -> Equipment {
    return equipment.reduce(("sum", 0, 0, 0)) { ("sum", $0.1 + $1.1, $0.2 + $1.2, $0.3 + $1.3) }
}

let playerHP = 100, bossHP = 104, bossDamage = 8, bossArmor = 1
func fightBoss(_ equipment: Equipment) -> Bool {
    var b = bossHP, p = playerHP
    while true {
        b -= max(1, equipment.2 - bossArmor)
        if b <= 0 { return true }
        p -= max(1, bossDamage - equipment.3)
        if p <= 0 { return false }
    }
}


// Part 1

var lowestCost = Int.max
for w in 0..<weapons.count {
    for a in 0..<armor.count {
        let zeroRingEquipment = [weapons[w], armor[a]]
        let zeroRingEquipmentSum = sumEquipment(zeroRingEquipment)
        if zeroRingEquipmentSum.1 < lowestCost && fightBoss(zeroRingEquipmentSum) {
            lowestCost = zeroRingEquipmentSum.1
            print(lowestCost, ":", zeroRingEquipment)
        }

        for r1 in 0..<rings.count {
            let oneRingEquipment = [weapons[w], armor[a], rings[r1]]
            let oneRingEquipmentSum = sumEquipment(oneRingEquipment)
            if oneRingEquipmentSum.1 < lowestCost && fightBoss(oneRingEquipmentSum) {
                lowestCost = oneRingEquipmentSum.1
                print(lowestCost, ":", oneRingEquipment)
            }

            for r2 in (r1+1)..<rings.count {
                let twoRingEquipment = [weapons[w], armor[a], rings[r1], rings[r2]]
                let twoRingEquipmentSum = sumEquipment(twoRingEquipment)
                if twoRingEquipmentSum.1 < lowestCost && fightBoss(twoRingEquipmentSum) {
                    lowestCost = twoRingEquipmentSum.1
                    print(lowestCost, ":", twoRingEquipment)
                }
            }
        }
    }
}


// Part 2

print("~~~~~")

var highestCost = Int.min
for w in 0..<weapons.count {
    for a in 0..<armor.count {
        let zeroRingEquipment = [weapons[w], armor[a]]
        let zeroRingEquipmentSum = sumEquipment(zeroRingEquipment)
        if zeroRingEquipmentSum.1 > highestCost && !fightBoss(zeroRingEquipmentSum) {
            highestCost = zeroRingEquipmentSum.1
            print(highestCost, ":", zeroRingEquipment)
        }

        for r1 in 0..<rings.count {
            let oneRingEquipment = [weapons[w], armor[a], rings[r1]]
            let oneRingEquipmentSum = sumEquipment(oneRingEquipment)
            if oneRingEquipmentSum.1 > highestCost && !fightBoss(oneRingEquipmentSum) {
                highestCost = oneRingEquipmentSum.1
                print(highestCost, ":", oneRingEquipment)
            }

            for r2 in (r1+1)..<rings.count {
                let twoRingEquipment = [weapons[w], armor[a], rings[r1], rings[r2]]
                let twoRingEquipmentSum = sumEquipment(twoRingEquipment)
                if twoRingEquipmentSum.1 > highestCost && !fightBoss(twoRingEquipmentSum) {
                    highestCost = twoRingEquipmentSum.1
                    print(highestCost, ":", twoRingEquipment)
                }
            }
        }
    }
}
