Day 21: RPG Simulator 20XX
==========================

http://adventofcode.com/day/21

Given fake RPG mechanics and an equipment list, find the cheapest equipment set that will let you beat a boss and the most expensive equipment set that will let you lose to the boss.

This one's yet another puzzle that I brute force by generating all equipment sets and testing them out one by one. However, unlike the other puzzles where I came up with a clever recursive generator, this one I just jumped directly into for loop and copy-paste hell. It's real gross.
