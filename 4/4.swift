#!/usr/bin/env xcrun swift

//: Playground - noun: a place where people can play

import Cocoa

func md5(input: String) -> String {
    let task = Process()
    task.launchPath = "/sbin/md5"
    task.arguments = ["-s", input]

    let pipe = Pipe()
    task.standardOutput = pipe

    task.launch()

    let fileHandle = pipe.fileHandleForReading
    let data = fileHandle.readDataToEndOfFile()
    fileHandle.closeFile()
    let s = String(data: data, encoding: String.Encoding.utf8)!
    var hash = s.components(separatedBy: " = ")[1]
    hash.remove(at: hash.index(before: hash.endIndex))
    return hash
}

let input = "ckczppom"


// Part 1

/*
var i = 110000
while true {
    if i % 1000 == 0 {
        print(String(format: "%d", i))
    }
    let hash = md5(input: String(format: "%@%d", input, i))
    if "00000" == hash[..<hash.index(hash.startIndex, offsetBy: 5)] {
        print(String(format: "%d -> %@", i, hash))
        break
    }
    i += 1
}
*/


// Part 2

var i = 3930000
while true {
    if i % 1000 == 0 {
        print(String(format: "%d", i))
    }
    let hash = md5(input: String(format: "%@%d", input, i))
    if "000000" == hash[..<hash.index(hash.startIndex, offsetBy: 6)] {
        print(String(format: "%d -> %@", i, hash))
        break
    }
    i += 1
}
