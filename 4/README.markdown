Day 4: The Ideal Stocking Stuffer
=================================

http://adventofcode.com/day/4

This solution is garbage, just ignore it.

Given a string, find the smallest integer such that, if you append the integer to the end of the string and take the MD5 hash of the result, the hash starts with 5 (for part 1) or 6 (for part 2) zeroes.

Rather than spend hours figuring out how to access CommonCrypto in Swift or spend hours implementing MD5 from scratch, I decided it would be a great idea to just NSTask (Process) out to `/sbin/md5`.

It turns out this is a terrible awful horrible idea when you have to do it nearly four million times. It would slow down after a while, so I had it output its progress so that I could I periodically kill it, update the script to start at the last milestone it printed, and resume it. And I went to bed before it finished solving the second part so I left it on overnight, and when I came back it had straight up crashed.

It probably would have been faster if I had just implemented MD5 from scratch.
