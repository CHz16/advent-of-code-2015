#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "9.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


func permutations<T>(_ l: [T]) -> [[T]] {
    if l.count == 0 {
        return [[]]
    }

    var perms: [[T]] = []
    for i in 0..<l.count {
        let l2 = Array(l[0..<i] + l[i+1..<l.count])
        perms += permutations(l2).map { $0 + [l[i]] }
    }
    return perms
}


var distances: Dictionary<String, Dictionary<String, Int>> = [:]

for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    let city1 = components[0], city2 = components[2], distance = Int(components[4])!

    if distances[city1] == nil {
        distances[city1] = [:]
    }
    if distances[city2] == nil {
        distances[city2] = [:]
    }
    distances[city1]?[city2] = distance
    distances[city2]?[city1] = distance
}

var routes = permutations(Array(distances.keys))


// Parts 1 & 2

var minRouteDistance = Int.max
var maxRouteDistance = 0
for route in routes {
    var distance = 0
    for i in 0..<(route.count - 1) {
        distance += (distances[route[i]]?[route[i + 1]])!
    }
    if distance < minRouteDistance {
        minRouteDistance = distance
    }
    if distance > maxRouteDistance {
        maxRouteDistance = distance
    }
}
print(minRouteDistance)
print(maxRouteDistance)
