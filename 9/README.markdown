Day 9: All in a Single Night
============================

http://adventofcode.com/day/9

And now it's traveling salesman. Here we just brute force the heck out of it: generate all possible routes and find the minimum and maximum distances.

The recursive permutation generator is probably the most interesting part of this solution. It works by finding all the permutations that end with the first element in the list, the second element, the third element, and so on, and returning the union of all those lists. To find list of all permutations ending with element X, we simply remove X from the list, find all the permutations of that shorter list, and then add X to the end of each of those permutations.

This is the first of several [SICP](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs)-inspired recursive generators that I used to brute force problems.
