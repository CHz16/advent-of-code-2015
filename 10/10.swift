#!/usr/bin/env xcrun swift

import Cocoa

func lookAndSay(_ input: String) -> String {
    var output = ""

    var current: Character = " ", count = 0
    for c in input {
        if c != current {
            if count != 0 {
                output += String(count)
                output += String(current)
            }
            current = c
            count = 0
        }
        count += 1
    }

    output += String(count)
    output += String(current)
    return output
}

var s = "1321131112"
for i in 1...50 {
    s = lookAndSay(s)
    if (i == 40) {
        print(s.count)
    }
}
print(s.count)
