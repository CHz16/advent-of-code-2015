Day 10: Elves Look, Elves Say
=============================

http://adventofcode.com/day/10

Just a direct implementation of [look-and-say sequences](https://en.wikipedia.org/wiki/Look-and-say_sequence), nothing to comment on here really.
