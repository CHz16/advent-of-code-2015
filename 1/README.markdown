Day 1: Not Quite Lisp
=====================

http://adventofcode.com/day/1

Extremely simple string processing problem, just counting the number of times open parentheses and close parenthesis appear in a string. There's absolutely no cleverness whatsoever in my solution.
