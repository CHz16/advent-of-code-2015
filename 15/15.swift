#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "15.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

func sums(elements: Int, limit: Int) -> [[Int]] {
    if elements == 1 {
        return [[limit]]
    }

    var a: [[Int]] = []
    for i in 0...limit {
        a += sums(elements: elements - 1, limit: limit - i).map { [i] + $0 }
    }
    return a
}

struct Ingredient {
    let name: String, capacity: Int, durability: Int, flavor: Int, texture: Int, calories: Int
}

var ingredients: [Ingredient] = []
for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    ingredients.append(Ingredient(name: components[0], capacity: Int(components[2])!, durability: Int(components[4])!, flavor: Int(components[6])!, texture: Int(components[8])!, calories: Int(components[10])!))
}

var maxScore = Int.min
var max500Score = Int.min
for config in sums(elements: ingredients.count, limit: 100) {
    let cal = zip(ingredients, config).map { $0.0.calories * $0.1 }.reduce(0) { $0 + $1 }
    
    let cap = max(0, zip(ingredients, config).map { $0.0.capacity * $0.1 }.reduce(0) { $0 + $1 })
    let dur = max(0, zip(ingredients, config).map { $0.0.durability * $0.1 }.reduce(0) { $0 + $1 })
    let fla = max(0, zip(ingredients, config).map { $0.0.flavor * $0.1 }.reduce(0) { $0 + $1 })
    let tex = max(0, zip(ingredients, config).map { $0.0.texture * $0.1 }.reduce(0) { $0 + $1 })

    let score = cap * dur * fla * tex
    if score > maxScore {
        maxScore = score
    }
    if cal == 500 && score > max500Score {
        max500Score = score
    }
}
print(maxScore)
print(max500Score)
