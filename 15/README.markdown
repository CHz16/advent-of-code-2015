Day 15: Science for Hungry People
=================================

http://adventofcode.com/day/15

This one basically boils down to finding an array `[a, b, c, d]` such that `a + b + c + d = 100` and `f(a, b, c, d)` is maximized. I did it by brute force: generate all such arrays and check the score for each one.

The `sums(n, l)` generator produces an array of all arrays of `n` integers that sum to `l`. It works recursively by generating all arrays such that the first element is 0 and the rest of the array sums to `l`, then all arrays such that the first element is 1 and the rest of the array sums to `l - 1`, and so on.
