Day 23: Opening the Turing Lock
===============================

http://adventofcode.com/day/23

Implement a really simple fake VM. I basically just used the exact same code structure of my Synacor Challenge solution (see the top-level README for more), which was really overkill for this problem.

I got wrong answers for a long time and was incredibly frustrated about it, until I figured out that I was just an inattentive goofball. The machine has two instructions: `jie r, offset` and `jio r, offset`. `jie` jumps to `offset` if the value of register `r` is even, while `jio` jumps if the value of `r` is **one**. Not odd, **one**. Foiled by assumptions.
