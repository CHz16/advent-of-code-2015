//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


typealias Register = String
typealias Offset = Int
enum Instruction {
    case Halve(Register), Triple(Register), Increment(Register), Jump(Offset), JumpIfEven(Register, Offset), JumpIfOne(Register, Offset)
}


var instructions: [Instruction] = []
for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    switch components[0] {
    case "hlf":
        instructions.append(.Halve(components[1]))
    case "tpl":
        instructions.append(.Triple(components[1]))
    case "inc":
        instructions.append(.Increment(components[1]))
    case "jmp":
        instructions.append(.Jump(Int(components[1])!))
    case "jie":
        instructions.append(.JumpIfEven(components[1], Int(components[2])!))
    case "jio":
        instructions.append(.JumpIfOne(components[1], Int(components[2])!))
    default:
        break
    }
}


func execute(_ registers: [String: Int]) -> [String: Int] {
    var registers = registers

    var pc = 0
    while pc < instructions.count {
        switch instructions[pc] {
        case let .Halve(r):
            registers[r]! /= 2
            pc += 1
        case let .Triple(r):
            registers[r]! *= 3
            pc += 1
        case let .Increment(r):
            registers[r]! += 1
            pc += 1
        case let .Jump(o):
            pc += o
        case let .JumpIfEven(r, o):
            if registers[r]! % 2 == 0 {
                pc += o
            } else {
                pc += 1
            }
        case let .JumpIfOne(r, o):
            if registers[r]! == 1 {
                pc += o
            } else {
                pc += 1
            }
        }
    }

    return registers
}

print(execute(["a": 0, "b": 0]))
print(execute(["a": 1, "b": 0]))
