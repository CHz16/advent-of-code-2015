Day 7: Some Assembly Required
=============================

http://adventofcode.com/day/7

This was the first puzzle that required more than just looping through the input and doing some simple operations. Since the wire definitions in the input are in an arbitrary order, I build up the entire circuit and recursively calculate the signal of the requested wire with memoization. It takes significantly longer to construct the circuit in the playground than it does to actually do the calculations.

The Wire implementation would've been much cleaner if I'd gone with an enum with associated values instead (see day 23).
