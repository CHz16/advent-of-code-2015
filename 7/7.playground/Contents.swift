//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "input", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


class Circuit {
    var wires: [String: Wire] = [:]
    var signalCache: [String: Int] = [:]

    func addWire(named name: String, expression: String) {
        wires[name] = Wire(expression: expression)
    }

    func signalForWire(named name: String) -> Int? {
        if let signal = signalCache[name] {
            return signal
        }

        if let signal = wires[name]?.signalInCircuit(self) {
            signalCache[name] = signal
            return signal
        }

        if let signal = Int(name) {
            signalCache[name] = signal
            return signal
        }

        return nil
    }
}

struct Wire {
    let operation: WireOperation
    let leftWire: String
    let rightWire: String?

    enum WireOperation {
        case Constant, And, Or, Not, LeftShift, RightShift
    }

    init(expression: String) {
        let components = expression.components(separatedBy: " ")
        if expression.range(of: "AND") != nil {
            operation = .And
            leftWire = components[0]
            rightWire = components[2]
        } else if expression.range(of: "OR") != nil {
            operation = .Or
            leftWire = components[0]
            rightWire = components[2]
        } else if expression.range(of: "NOT") != nil {
            operation = .Not
            leftWire = components[1]
            rightWire = nil
        } else if expression.range(of: "LSHIFT") != nil {
            operation = .LeftShift
            leftWire = components[0]
            rightWire = components[2]
        } else if expression.range(of: "RSHIFT") != nil {
            operation = .RightShift
            leftWire = components[0]
            rightWire = components[2]
        } else {
            operation = .Constant
            leftWire = components[0]
            rightWire = nil
        }
    }

    func signalInCircuit(_ circuit: Circuit) -> Int? {
        var signal: Int?

        if let leftSignal = circuit.signalForWire(named: leftWire) {
            switch operation {
            case .Constant:
                signal = leftSignal
            case .Not:
                signal = ~leftSignal & 65535
            case .And, .Or, .LeftShift, .RightShift:
                if let rightSignal = circuit.signalForWire(named: rightWire!) {
                    switch operation {
                    case .And:
                        signal = leftSignal & rightSignal
                    case .Or:
                        signal = leftSignal | rightSignal
                    case .LeftShift:
                        signal = leftSignal << rightSignal & 65535
                    case .RightShift:
                        signal = leftSignal >> rightSignal
                    default:
                        break
                    }
                }
            }
        }

        return signal
    }
}


// Part 1

var circuit = Circuit()
for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " -> ")
    circuit.addWire(named: components[1], expression: components[0])
}
circuit.signalForWire(named: "a")


// Part 2

circuit.signalCache = [:]
circuit.signalCache["b"] = 956
circuit.signalForWire(named: "a")
