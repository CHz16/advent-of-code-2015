Day 19: Medicine for Rudolph
============================

http://adventofcode.com/day/19

Given a context-free grammar and a long string, first find how many distinct strings you can produce by applying one rule to the string, and second find the number of steps in the shortest derivation of the string.

The first part isn't too bad: I just jammed every single product into a set and let it sort out the uniqueness for me.

The second part was easily the hardest part of any puzzle up to this point. It took me an hour and twenty-two minutes to finish it, and that was good enough for 44th place. [If you look at the stats](http://adventofcode.com/stats), there's a huge jump in day 19 in the number of people who were able to complete the first part of the puzzle but not the second.

I solved the second part by using depth-first search to apply production rules to the string in reverse order until I reached the start symbol, kind of like a parser. If multiple derivations were possible, DFS would **not** have necessarily found the shortest solution, but [it turns out the grammar is structured in such a way that all derivations are the same length](https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/cy4p1td), so it was actually fine.
