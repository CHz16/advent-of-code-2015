#!/usr/bin/env xcrun swift

//: Playground - noun: a place where people can play

import Cocoa

let url = URL(fileURLWithPath: "19.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


var replacements: [(String, String)] = []
for line in input.components(separatedBy: "\n") {
    let components = line.components(separatedBy: " ")
    replacements.append((components[0], components[2]))
}

let medicine = "CRnSiRnCaPTiMgYCaPTiRnFArSiThFArCaSiThSiThPBCaCaSiRnSiRnTiTiMgArPBCaPMgYPTiRnFArFArCaSiRnBPMgArPRnCaPTiRnFArCaSiThCaCaFArPBCaCaPTiTiRnFArCaSiRnSiAlYSiThRnFArArCaSiRnBFArCaCaSiRnSiThCaCaCaFYCaPTiBCaSiThCaSiThPMgArSiRnCaPBFYCaCaFArCaCaCaCaSiThCaSiRnPRnFArPBSiThPRnFArSiRnMgArCaFYFArCaSiRnSiAlArTiTiTiTiTiTiTiRnPMgArPTiTiTiBSiRnSiAlArTiTiRnPMgArCaFYBPBPTiRnSiRnMgArSiThCaFArCaSiThFArPRnFArCaSiRnTiBSiThSiRnSiAlYCaFArPRnFArSiThCaFArCaCaSiThCaCaCaSiRnPRnCaFArFYPMgArCaPBCaPBSiRnFYPBCaFArCaSiAl"


// Part 1

var products = Set<String>()
for (before, after) in replacements {
    var range = medicine.startIndex..<medicine.endIndex
    while let match = medicine.range(of: before, options: [], range: range) {
        products.insert(medicine.replacingCharacters(in: match, with: after))
        range = match.upperBound..<medicine.endIndex
    }
}
print(products.count)


// Part 2

let target = "e"

func dfs(_ product: String) -> Int? {
    if product == target {
        return 0
    }
    if product.count < target.count {
        return nil
    }

    for (after, before) in replacements {
        var range = product.startIndex..<product.endIndex
        while let match = product.range(of: before, options: [], range: range) {
            let newProduct = product.replacingCharacters(in: match, with: after)
            if let n = dfs(newProduct) {
                return 1 + n
            }
            range = match.upperBound..<product.endIndex
        }
    }

    return nil
}

print(dfs(medicine)!)
